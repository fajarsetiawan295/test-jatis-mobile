package voucher

import (
	"test-agit/internal/model"

	"gorm.io/gorm"
)

type Repository interface {
	Create(e *CreateRequest) (*model.VoucherTransaksiModel, error)
	Find(IDTransaksi string) (*model.VoucherTransaksiModel, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db: db}
}

func (r *repository) Create(e *CreateRequest) (*model.VoucherTransaksiModel, error) {

	tx := r.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	var data model.VoucherTransaksiModel
	errMaster := tx.Debug().Model(data).Create(map[string]interface{}{
		"id_transaksi":     e.Id_transaksi,
		"amount_transaksi": e.AmountTransaksi,
		"voucher_amount":   e.VoucherAmount,
		"user_id":          e.UserId,
	}).Error
	if errMaster != nil {
		tx.Rollback()
		return nil, errMaster
	}

	tx.Commit()
	return &data, nil
}

func (r *repository) Find(IDTransaksi string) (*model.VoucherTransaksiModel, error) {

	var data model.VoucherTransaksiModel
	errMaster := r.db.Debug().Where("id_transaksi = ?", IDTransaksi).First(&data).Error
	if errMaster != nil {
		return nil, errMaster
	}

	return &data, nil
}
