package voucher

import (
	"errors"
	"test-agit/internal/model"
)

type Service interface {
	Create(payload *CreateRequest) (*model.VoucherTransaksiModel, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository: repository}
}

func (s *service) Create(payload *CreateRequest) (*model.VoucherTransaksiModel, error) {

	// cek Id transaksi
	_, errCek := s.repository.Find(payload.Id_transaksi)
	if errCek == nil {
		return nil, errors.New("Transaksi sudah di gunakan")
	}

	data, err := s.repository.Create(payload)
	if err != nil {
		return nil, err
	}

	return data, nil
}
