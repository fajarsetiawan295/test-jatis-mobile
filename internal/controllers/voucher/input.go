package voucher

//CREATE
type CreateRequest struct {
	MaxPembelanjaan int64  `json:"max_pembelanjaan" validate:"required"`
	GetVoucher      int64  `json:"get_voucher" validate:"required"`
	Id_transaksi    string `gorm:"type:varchar(100);not null;" json:"id_transaksi" validate:"required,max=100"`
	AmountTransaksi int64  `gorm:"not null;" json:"amount_transaksi" validate:"required"`
	VoucherAmount   int64  `gorm:"not null;" json:"voucher_amount"`
	UserId          uint   `gorm:"not null;" json:"user_id" validate:"required"`
}
