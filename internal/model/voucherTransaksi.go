package model

import (
	"test-agit/internal/abstraction"
)

type VoucherTransaksi struct {
	Id_transaksi    string `gorm:"type:varchar(100);not null;" json:"id_transaksi" validate:"required,max=100"`
	AmountTransaksi int64  `gorm:"not null;" json:"amount_transaksi" validate:"required"`
	VoucherAmount   int64  `gorm:"not null;" json:"voucher_amount" validate:"required"`
	UserId          uint   `gorm:"not null;" json:"user_id" validate:"required"`
}

type VoucherTransaksiModel struct {
	abstraction.EntityAI
	VoucherTransaksi
	abstraction.Filter
}

func (VoucherTransaksiModel) TableName() string {
	return "voucher_transaksi"
}
