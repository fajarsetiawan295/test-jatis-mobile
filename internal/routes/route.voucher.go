package routes

import (
	voucher "test-agit/internal/controllers/voucher"
	handlerVoucher "test-agit/internal/handlers/voucher"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func InitVoucher(db *gorm.DB, route *gin.Engine) {

	repository := voucher.NewRepository(db)
	service := voucher.NewService(repository)
	handler := handlerVoucher.NewHandler(service)

	gRoute := route.Group("/api/v1/voucher")

	gRoute.POST("/create", handler.Store)

}
