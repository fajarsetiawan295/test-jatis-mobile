package handlerVoucher

import (
	util "test-agit/helpers/utils"
	voucher "test-agit/internal/controllers/voucher"

	"github.com/gin-gonic/gin"
)

type handler struct {
	service voucher.Service
}

func NewHandler(service voucher.Service) *handler {
	return &handler{service: service}
}

func (h *handler) Store(ctx *gin.Context) {

	var payload voucher.CreateRequest
	err := ctx.ShouldBindJSON(&payload)
	if err != nil {
		util.ERROR(ctx, err)
		return
	}

	var message string
	if payload.AmountTransaksi >= payload.MaxPembelanjaan {
		message = "mendapatkan voucher"
		payload.VoucherAmount = payload.GetVoucher
	} else {
		payload.VoucherAmount = 0
		message = "tidak mendapatkan voucher"
	}

	validate := payload.Validator()
	if validate != "" {
		util.FAILED(ctx, validate)
		return
	}

	_, err = h.service.Create(&payload)
	if err != nil {
		util.ERROR(ctx, err)
		return
	}

	util.JSON(ctx, message, nil)
	return
}
